FROM openjdk:8-jre-alpine
COPY ./target/restful_webservice-1.0.0-SNAPSHOT.jar
WORKDIR /usr/src/hola
EXPOSE 8080
CMD ["java", "-jar", "restful_webservice-1.0.0-SNAPSHOT.jar"]