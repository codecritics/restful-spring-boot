-- Populate data in the Users tables
insert into user values(10001, sysdate(), 'Tanawa Tsamo Marius');
insert into user values(10002, sysdate(), 'Tanawa Noumo Grâce');
insert into user values(3, sysdate(), 'Tanawa Thanckoué Philippe');

-- Populate data in the Posts Tables

insert into post values (11001, 'My first Post', 10001)
insert into post values (11002, 'My second Post', 10002)