package com.codecritics.restfulws.filtering;

        import com.fasterxml.jackson.annotation.JsonFilter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import org.springframework.web.bind.annotation.GetMapping;

@JsonFilter("SomeBeanFilter")
public class SomeBean {
    public String field1;
    public String field2;
    public String field3;

    public SomeBean(String field1, String field2, String field3) {
        super();
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
    }
}
